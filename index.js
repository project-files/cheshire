#! /usr/bin/env node
var argv = require('minimist')(process.argv.slice(2));
var path = require('path');
var sh = require('shelljs');
var inquirer = require('inquirer');
var chalk = require('chalk');
var options = require('./cheshire.options.js');
var program = require('./lib/index.js');

function run(){
  program( options ).catch(e=>{ console.error( e ) });
}

var questions = [
  {
    type: 'confirm',
    name: 'runGenerator',
    message: 'Do you want to generate the project?',
    default: false
  },
];
var dir = path.resolve( argv._[0]||options.failsafe );
options.source = dir;
var templateHtml = path.join( dir, 'template.html' );

//console.log("DIR", dir)

if( sh.test('-d', dir ) ){
 //console.log("Directory is OK: %s", dir);
  if( sh.test('-f', templateHtml ) ){
   // console.log("Template File is OK: %s", templateHtml);
    // console.log("argv.yes:", argv.yes);
    if(argv.force === true){
      // non interactive use
      run()
    }else{
      inquirer.prompt(questions, function (answers) {
        if( answers.runGenerator === true ) run()
      });
    }
  }

}else{
  console.log('You must specify a source directory containing a theme.')
  console.log('The theme must contain a theme.html file.')
}
