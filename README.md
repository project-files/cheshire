---
title: Cheshire
repo: https://gitlab.com/project-files/cheshire
homepage: https://gitlab.com/project-files/cheshire
language: JavaScript
license: MIT
templates: cheerio
description: The best way of writing
---

### Installation

    please install with the -g flag
    you will need generator-cheshire

### Developers

  npm init
  npm link generator-cheshire
  yo cheshire 


### Usage

    execute ```cheshire``` in the cheshire project directory

### Developing

    babel --watch src -d lib

> Cheshire does just one thing. it converts all page/.json files into html using the initial template.html it finds in your webpack project.

### The Ideas ###
- Convert .md files to html code and inject it into html documents.
- Markdown, because it is simple.
- JavaScript formatted configuration file because it is more flexible than YAML.
- Uses existing webpack based project that provides all the HTML files (theme).
- Injects a local layout file into pre-bundled template.html and fill in the blanks

### The Problem ###

> Everything the Static Generator gave me
  fell to pieces the moment I touched
  the theme file.

- I needed a static site generator.
- I wrote my content in Markdown.
- The Theme Was Ugly and I tried to fix it.
- The theme was a separate project far away.
- I needed to work on the UI and it needed a UI resources bundler.

### The Solution ###

> I got rid of the Static Generator
  that led me back to complexity and
  wrote Cheshire.

- Cheshire depends on an existing webpack project.
- The webpack project must generate a template.html file.
- Cheshire will inject template.html with Layout and Module Positions
- /pages/page-name.json will seed it with content creating page-name.html
- Newly generated project files are merged with a copy of initial webpack project.
- Everything is already optimized.
- :shipit:

### Ideas ###
Yeoman Menu Administration Panel
