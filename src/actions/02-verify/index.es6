const sh = require('shelljs');
const path = require('path');
module.exports = async function(options){
  if(!sh.test('-d', './pages')) throw new Error(`Missing Pages Directory # mkdir ${path.resolve('./pages')}`);
};
