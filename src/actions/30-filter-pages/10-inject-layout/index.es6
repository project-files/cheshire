const cheerio = require('cheerio');
module.exports = function(i){
  //console.log("Inject layout");
    // Create DOM
    let $ = cheerio.load(i.page.html);

    // Insert Template
    $('body').prepend( i.page.layout );

    // Serialize
    i.page.html = $.html();

    return i;
}
