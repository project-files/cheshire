const cheerio = require('cheerio');

module.exports = function(i){

  //console.log("Inject Includes")

  // Create DOM
  let $ = cheerio.load(i.page.html);

  // Inject Includes
  i.configuration.content.forEach(item => {
    let [fileName, selector] = item.split('@');
    $('.' + selector).append(`<div data-include="content/${fileName}.md"></div>`);
  });

  // Serialize HTML
  i.page.html = $.html();

  return i;
}
