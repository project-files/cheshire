const path = require('path');
const cheerio = require('cheerio');
const marked = require("marked");
const sanitize = require("sanitize-filename");
const startsWith = require('lodash/startsWith');
const sh = require('shelljs');

marked.setOptions({
  renderer: new marked.Renderer(),
  gfm: true,
  tables: true,
  breaks: false,
  pedantic: false,
  sanitize: false,
  smartLists: true,
  smartypants: false
});


module.exports = function(i){
  // console.log("Interpolate Includes")
// Create DOM
let $ = cheerio.load(i.page.html);

// Inject Files
$('[data-include]').each((index, element) => {
  let file = $(element).data('include').split("/").filter(i=>i.length).map(i=>sanitize( i )).join("/");
  if(file){
    let resolved = path.resolve( file );
    if(startsWith(resolved, path.resolve("."))) {
      if(sh.test('-f', resolved)){
        if(resolved.match(/\.md$/)){
          let data = sh.cat( resolved );
          data = marked(data);
          $(element).append(data);
        } else if (resolved.match(/\.html$/)){
          let data = sh.cat( resolved );
          $(element).append(data);
        }else{
          // unsupported extension
        }
      }
      $(element).attr('data-include', null);
    }
  }
});

// Serialize HTML
i.page.html = $.html();
return i;

}
