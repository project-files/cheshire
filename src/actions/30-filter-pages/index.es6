const sh = require('shelljs');
const path = require("path");
const flatten = require('flat')
const extend = require("extend");

const inject_layout = require('./10-inject-layout/index.js');
const inject_includes_into_positions = require('./15-inject-includes-into-positions/index.js');
const interpolate_data_variables = require('./20-interpolate-data-variables/index.js');
const interpolate_data_includes = require('./30-interpolate-data-includes/index.js');

module.exports = async function(options){



  // list json files in pages
  sh.ls(options.pages)

  // prepare paths
  .map(i => {
    let paths = {};
    paths.source = path.resolve(path.join(options.pages, i));
    paths.destination = path.resolve(path.join(options.destination, path.basename(i, '.json') + '.html'));
    return paths;
  })

  // Create Metadata
  .map(i => {

    let variables = extend(true, {}, options.variables, JSON.parse( sh.cat( i.source ) ));
    let metatata = {};

    metatata.configuration = variables;
    metatata.variables = flatten(variables);
    metatata.options = options;

    metatata.page = {};
    metatata.page.source = i.source;
    metatata.page.destination = i.destination;
    metatata.page.layout = sh.cat(path.join(options.layouts, variables.layout));
    metatata.page.html = sh.cat(path.join(options.destination, options.templateFileName));

    return metatata;
  })

  // only for published
  .filter(i => i.variables.published)

  .map(inject_layout)
  .map(inject_includes_into_positions)
  .map(interpolate_data_variables)
  .map(interpolate_data_includes)

  // Cleanup
  .map(i => {
    return {
      destination: i.page.destination,
      html: i.page.html,
    };
  })

  // Save
  .map(i => {
    i.html.to(i.destination);
  });



};
