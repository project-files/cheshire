var sh = require('shelljs');
var path = require('path');
module.exports = async function(options){

  var srcDir = "";


  if(sh.test('-d', options.source)) srcDir = options.source;
  if(!sh.test('-d', options.failsafe)) srcDir = options.failsafe;

  sh.cp("-R", srcDir + path.sep, options.destination + path.sep)

};
