var sh = require('shelljs');
var path = require('path');
module.exports = async function(options){
  sh.cp("-R", options.merge + path.sep, options.destination + path.sep);
};
