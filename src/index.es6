require("babel-polyfill");
const path = require("path");
function timeout(ms) { return new Promise(resolve => setTimeout(resolve, ms)); }

const verify = require("./actions/02-verify/index.js");
const prepare = require("./actions/05-prepare/index.js");
const initial_copy = require("./actions/10-initial-copy/index.js");
const merge = require("./actions/15-merge/index.js");
const filter_pages = require("./actions/30-filter-pages/index.js");

module.exports = async options => {

  await verify(options);
  await prepare(options);
  await initial_copy(options);
  await merge(options);
  await filter_pages(options);


}
