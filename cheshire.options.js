var path = require("path");

module.exports = {

  title: "Cheshire Project",

  failsafe: path.resolve(path.join(__dirname , 'failsafe')),
  layouts: path.resolve('./layout/'),

  templateFileName: 'template.html',
  source: null,
  destination: path.resolve('./public'),
  pages: path.resolve('./pages'),
  merge: path.resolve('./merge'),

  variables: {
    layout: "default.html"
  }

};
