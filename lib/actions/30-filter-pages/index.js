'use strict';

function _asyncToGenerator(fn) { return function () { var gen = fn.apply(this, arguments); return new Promise(function (resolve, reject) { function step(key, arg) { try { var info = gen[key](arg); var value = info.value; } catch (error) { reject(error); return; } if (info.done) { resolve(value); } else { return Promise.resolve(value).then(function (value) { return step("next", value); }, function (err) { return step("throw", err); }); } } return step("next"); }); }; }

var sh = require('shelljs');
var path = require("path");
var flatten = require('flat');
var extend = require("extend");

var inject_layout = require('./10-inject-layout/index.js');
var inject_includes_into_positions = require('./15-inject-includes-into-positions/index.js');
var interpolate_data_variables = require('./20-interpolate-data-variables/index.js');
var interpolate_data_includes = require('./30-interpolate-data-includes/index.js');

module.exports = function () {
  var ref = _asyncToGenerator(regeneratorRuntime.mark(function _callee(options) {
    return regeneratorRuntime.wrap(function _callee$(_context) {
      while (1) {
        switch (_context.prev = _context.next) {
          case 0:

            // list json files in pages
            sh.ls(options.pages)

            // prepare paths
            .map(function (i) {
              var paths = {};
              paths.source = path.resolve(path.join(options.pages, i));
              paths.destination = path.resolve(path.join(options.destination, path.basename(i, '.json') + '.html'));
              return paths;
            })

            // Create Metadata
            .map(function (i) {

              var variables = extend(true, {}, options.variables, JSON.parse(sh.cat(i.source)));
              var metatata = {};

              metatata.configuration = variables;
              metatata.variables = flatten(variables);
              metatata.options = options;

              metatata.page = {};
              metatata.page.source = i.source;
              metatata.page.destination = i.destination;
              metatata.page.layout = sh.cat(path.join(options.layouts, variables.layout));
              metatata.page.html = sh.cat(path.join(options.destination, options.templateFileName));

              return metatata;
            })

            // only for published
            .filter(function (i) {
              return i.variables.published;
            }).map(inject_layout).map(inject_includes_into_positions).map(interpolate_data_variables).map(interpolate_data_includes)

            // Cleanup
            .map(function (i) {
              return {
                destination: i.page.destination,
                html: i.page.html
              };
            })

            // Save
            .map(function (i) {
              i.html.to(i.destination);
            });

          case 1:
          case 'end':
            return _context.stop();
        }
      }
    }, _callee, this);
  }));

  return function (_x) {
    return ref.apply(this, arguments);
  };
}();