'use strict';

var _slicedToArray = function () { function sliceIterator(arr, i) { var _arr = []; var _n = true; var _d = false; var _e = undefined; try { for (var _i = arr[Symbol.iterator](), _s; !(_n = (_s = _i.next()).done); _n = true) { _arr.push(_s.value); if (i && _arr.length === i) break; } } catch (err) { _d = true; _e = err; } finally { try { if (!_n && _i["return"]) _i["return"](); } finally { if (_d) throw _e; } } return _arr; } return function (arr, i) { if (Array.isArray(arr)) { return arr; } else if (Symbol.iterator in Object(arr)) { return sliceIterator(arr, i); } else { throw new TypeError("Invalid attempt to destructure non-iterable instance"); } }; }();

var cheerio = require('cheerio');

module.exports = function (i) {

  //console.log("Inject Includes")

  // Create DOM
  var $ = cheerio.load(i.page.html);

  // Inject Includes
  i.configuration.content.forEach(function (item) {
    var _item$split = item.split('@');

    var _item$split2 = _slicedToArray(_item$split, 2);

    var fileName = _item$split2[0];
    var selector = _item$split2[1];

    $('.' + selector).append('<div data-include="content/' + fileName + '.md"></div>');
  });

  // Serialize HTML
  i.page.html = $.html();

  return i;
};