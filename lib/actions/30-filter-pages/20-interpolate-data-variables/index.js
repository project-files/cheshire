'use strict';

var cheerio = require('cheerio');

module.exports = function (i) {
  // console.log("Interpolate Variables")

  // Create DOM
  var $ = cheerio.load(i.page.html);

  // Inject Variables
  $('[data-variable]').each(function (index, element) {
    var key = $(element).data('variable');
    if (i.variables[key]) $(element).append(i.variables[key]);
    $(element).attr('data-variable', null);
  });

  // Serialize HTML
  i.page.html = $.html();

  return i;
};