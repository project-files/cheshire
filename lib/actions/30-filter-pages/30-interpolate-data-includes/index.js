'use strict';

var path = require('path');
var cheerio = require('cheerio');
var marked = require("marked");
var sanitize = require("sanitize-filename");
var startsWith = require('lodash/startsWith');
var sh = require('shelljs');

marked.setOptions({
  renderer: new marked.Renderer(),
  gfm: true,
  tables: true,
  breaks: false,
  pedantic: false,
  sanitize: false,
  smartLists: true,
  smartypants: false
});

module.exports = function (i) {
  // console.log("Interpolate Includes")
  // Create DOM
  var $ = cheerio.load(i.page.html);

  // Inject Files
  $('[data-include]').each(function (index, element) {
    var file = $(element).data('include').split("/").filter(function (i) {
      return i.length;
    }).map(function (i) {
      return sanitize(i);
    }).join("/");
    if (file) {
      var resolved = path.resolve(file);
      if (startsWith(resolved, path.resolve("."))) {
        if (sh.test('-f', resolved)) {
          if (resolved.match(/\.md$/)) {
            var data = sh.cat(resolved);
            data = marked(data);
            $(element).append(data);
          } else if (resolved.match(/\.html$/)) {
            var _data = sh.cat(resolved);
            $(element).append(_data);
          } else {
            // unsupported extension
          }
        }
        $(element).attr('data-include', null);
      }
    }
  });

  // Serialize HTML
  i.page.html = $.html();
  return i;
};