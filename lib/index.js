"use strict";

function _asyncToGenerator(fn) { return function () { var gen = fn.apply(this, arguments); return new Promise(function (resolve, reject) { function step(key, arg) { try { var info = gen[key](arg); var value = info.value; } catch (error) { reject(error); return; } if (info.done) { resolve(value); } else { return Promise.resolve(value).then(function (value) { return step("next", value); }, function (err) { return step("throw", err); }); } } return step("next"); }); }; }

require("babel-polyfill");
var path = require("path");
function timeout(ms) {
  return new Promise(function (resolve) {
    return setTimeout(resolve, ms);
  });
}

var verify = require("./actions/02-verify/index.js");
var prepare = require("./actions/05-prepare/index.js");
var initial_copy = require("./actions/10-initial-copy/index.js");
var merge = require("./actions/15-merge/index.js");
var filter_pages = require("./actions/30-filter-pages/index.js");

module.exports = function () {
  var ref = _asyncToGenerator(regeneratorRuntime.mark(function _callee(options) {
    return regeneratorRuntime.wrap(function _callee$(_context) {
      while (1) {
        switch (_context.prev = _context.next) {
          case 0:
            _context.next = 2;
            return verify(options);

          case 2:
            _context.next = 4;
            return prepare(options);

          case 4:
            _context.next = 6;
            return initial_copy(options);

          case 6:
            _context.next = 8;
            return merge(options);

          case 8:
            _context.next = 10;
            return filter_pages(options);

          case 10:
          case "end":
            return _context.stop();
        }
      }
    }, _callee, undefined);
  }));

  return function (_x) {
    return ref.apply(this, arguments);
  };
}();